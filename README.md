<!-- ABOUT THE PROJECT -->
## About The Project
link to golang structure best practises → https://github.com/golang-standards/project-layout

<!-- DEVELOPMENT -->
## Development

### Building locally

```shell
docker build -t catfish -f build/Dockerfile .
```

### Running locally

```shell
docker run -p 80:8080 catfish
```

<!-- TODO -->
## TODO
This Readme as well as the whole project is a WIP
